export interface Information {

    id: number;
    temperature: string;
    wind: string;
    humidity: string;
    pressure: string;
}
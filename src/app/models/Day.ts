export interface Day {

    id: number;
    date: Date;
    temperature_max: number;
    temperature_min: number;
    text: string;
    humidity: number;
    wind: number;
    wind_direction: string;
    sunrise: Date;
    sunset: Date;
    moonrise: Date;
    moonset: Date;
}
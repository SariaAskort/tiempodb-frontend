import { Day } from './Day';
import { Information } from './Information';

export class Locality {
    id: number;
    name: string;
    country: string;
    days: Day[];
    information: Information;

    constructor(
        id: number,
        name: string,
        country: string,
        days: Day[],
        information: Information
    ) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.days = days;
        this.information = information;
    }
}
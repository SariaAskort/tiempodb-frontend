import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'DayFilterPipe' })
export class DayFilterPipe implements PipeTransform {
  /**
   * Transform
   *
   * @param {any[]} items
   * @param {string} searchText
   * @returns {any[]}
   */
  transform(items: any[], searchText: string): any[] {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }
    searchText = searchText.toLocaleLowerCase();

    return items.filter(it => {
      return it.date.toLocaleLowerCase().includes(searchText)
        || it.text.toLocaleLowerCase().includes(searchText);
    });
  }
}
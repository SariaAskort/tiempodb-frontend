import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { LocalityListComponent } from './locality-list/locality-list.component';
import { LocalityComponent } from './locality/locality.component';
import { LocalityFilterPipe } from './locality-filter.pipe';
import { DayFilterPipe } from './day-filter.pipe';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LocalityListComponent,
    LocalityComponent,
    LocalityFilterPipe,
    DayFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

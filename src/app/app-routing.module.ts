import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LocalityListComponent } from './locality-list/locality-list.component';
import { LocalityComponent } from './locality/locality.component';

const routes: Routes = [
  {
    path: "", 
    component: LocalityListComponent
  },
  {
    path: "locality/:id",
    component: LocalityComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

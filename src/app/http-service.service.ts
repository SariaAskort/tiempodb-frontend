import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Locality } from './models/Locality';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor( private http: HttpClient ) { }

  getLocalities() {
    return this.http.get<Observable<Locality[]>>('http://localhost:8080/api/v1/localities/all');
  }

  getLocality(id: number): Observable<Locality> {
    let params = new HttpParams();
    params.set("localityId", id.toString());
    return this.http.get<Locality>("http://localhost:8080/api/v1/localities/" + id);
  }

  removeLocality(id: number) {
    return this.http.get("http://localhost:8080/api/v1/localities/" + id + "/delete/",{});
  }

  retrieveNewDays(id: number) {
    return this.http.get<Locality>("http://localhost:8080/api/v1/localities/" + id + "/retrieve", {});
  }
}

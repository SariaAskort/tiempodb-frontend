import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpService } from '../http-service.service';
import { Locality } from '../models/Locality';

@Component({
  selector: 'app-locality-list',
  templateUrl: './locality-list.component.html',
  styleUrls: ['./locality-list.component.css']
})
export class LocalityListComponent implements OnInit {

  localities: any;
  filterText: string = '';

  constructor(
    private _http: HttpService
    ) { }

  ngOnInit(): void {
    this._http.getLocalities().subscribe(data => {
      this.localities = data;
      console.log(this.localities);
    });
  }

  delete(id: number) {
    this._http.removeLocality(id).subscribe(data => {
      this.localities = data;
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


import { HttpService } from '../http-service.service';

@Component({
  selector: 'app-locality',
  templateUrl: './locality.component.html',
  styleUrls: ['./locality.component.css']
})
export class LocalityComponent implements OnInit {

  id: number = -1;
  locality: any;
  filterText: string = '';

  constructor(
    private route: ActivatedRoute,
    private _http: HttpService,
    private router: Router
    ) {
      this.id = this.route.snapshot.params.id;
    }

  ngOnInit(): void {
    this._http.getLocality(this.id).subscribe(data => {
      this.locality = data;
      console.log(this.locality);
    }).closed;
  }

  retrieveNewDays(id: number) {
    this._http.retrieveNewDays(id).subscribe(() => {
      this.router.navigate(['locality', id]);
    });
  }
}
